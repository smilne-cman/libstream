#ifndef __libstream__
#define __libstream__

/* Includes *******************************************************************/
#include <libcollection/list.h>
#include "types.h"

/* Types **********************************************************************/

/* Macros *********************************************************************/

/* Global Functions ***********************************************************/
Stream *stream_new();

void stream_add(Stream *stream, void *data);
void stream_fork(Stream *stream, Sink *sink);

Stream *stream_transform(Stream *stream, StreamTransformer transformer, void *options);
Stream *stream_filter(Stream *stream, ConditionTransformer condition, void *options);
List *stream_split(Stream *stream, ConditionTransformer condition, void *options);
Stream *stream_join(Stream *stream1, Stream *stream2);
Stream *stream_batch(Stream *stream, int size);

void stream_destroy(Stream *stream);

#endif
