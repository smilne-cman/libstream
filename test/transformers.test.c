#include <stdio.h>
#include <stdlib.h>
#include <libtest/libtest.h>
#include "transformers.h"

/* Types **********************************************************************/
typedef struct TestItem {
  int value;
} TestItem;

typedef struct GreaterThanOptions {
  int limit;
} GreaterThanOptions;

/* Prototypes *****************************************************************/
void test_conditional();
void test_batch();

static int greater_than(TestItem *data, GreaterThanOptions *options);
static TestItem *item_new(int value);
static GreaterThanOptions *options_new(int limit);

/* Global Variables ***********************************************************/

int main(int argc, char *argv[]) {
  test_init("transformers", argc, argv);

  test("Conditional Transformer", test_conditional);
  test("Batch Transformer", test_batch);

  return test_complete();
}

void test_conditional() {
  FilterTransformerOptions *options = filter_transformer_options(greater_than, options_new(10));

  TestItem *item1 = item_new(5);
  TestItem *item2 = item_new(15);

  test_assert( filter_transformer(item1, options) == NULL, "Should return NULL when condition is false" );
  test_assert( filter_transformer(item2, options) == item2, "Should return item when condition is true" );

  free(item1);
  free(item2);
  free(options);
}

void test_batch() {
  BatchTransformerOptions *options = batch_transformer_options(2);

  List *result1 = batch_transformer("hello", options);
  List *result2 = batch_transformer("world", options);

  test_assert( result1 == NULL, "Should not return items before the limit" );
  test_assert( result2 != NULL, "Should return a list when limit reached");
  test_assert_int( list_size(result2), 2, "Should return list with all items");
  test_assert_string( list_gets(result2, 0), "hello", "Should contain first item");
  test_assert_string( list_gets(result2, 1), "world", "Should contain second item");

  free(options);
  free(result2);
}

static int greater_than(TestItem *data, GreaterThanOptions *options) {
  return data->value > options->limit;
}

static TestItem *item_new(int value) {
  TestItem *item = (TestItem *)malloc(sizeof(TestItem));

  item->value = value;

  return item;
}

static GreaterThanOptions *options_new(int limit) {
  GreaterThanOptions *options = (GreaterThanOptions *)malloc(sizeof(GreaterThanOptions));

  options->limit = limit;

  return options;
}
