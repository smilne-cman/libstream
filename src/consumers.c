#include <stdio.h>
#include <stdlib.h>
#include <libcollection/list.h>
#include <libcollection/set.h>
#include "stream.h"
#include "consumers.h"
#include "types.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/
void memory_sink_consumer(void *data, List *store) {
  list_add(store, data);
}

void merge_sink_consumer(void *data, Stream *merge) {
  stream_add(merge, data);
}

void split_sink_consumer(void *data, SplitSink *options) {
  if (options->condition(data, options->options)) {
    stream_add(options->pass, data);
  } else {
    stream_add(options->fail, data);
  }
}

void transform_sink_consumer(void *data, TransformSink *options) {
  stream_add(options->output, options->transformer(data, options->options));
}
