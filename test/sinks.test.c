#include <stdio.h>
#include <stdlib.h>
#include <libtest/libtest.h>
#include <libcollection/list.h>
#include "sinks.h"
#include "stream.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
static void test_memory_sink();
static void test_transform_sink();
static void test_split_sink();
static void test_sink_compare();

static const char *prefix_transformer(const char *data, const char *options);
static void *null_transformer(void *data, void *options);
static int always_false(void *data, void *options);
static int always_true(void *data, void *options);

/* Global Variables ***********************************************************/

int main(int argc, char *argv[]) {
  test_init("sinks", argc, argv);

  test("Memory Sink", test_memory_sink);
  test("Transform Sink", test_transform_sink);
  test("Split Sink", test_split_sink);
  test("Compare", test_sink_compare);

  return test_complete();
}

static void test_memory_sink() {
  List *store = list_new(ListAny);
  Sink *sink = memory_sink(store);

  test_assert( sink->options == store, "Should pass store into consumer" );

  sink->consume("A", sink->options);
  sink->consume("B", sink->options);

  test_assert_int( list_size(store), 2, "Should add all items to the store" );
  test_assert_string( list_gets(store, 0), "A", "Should add the first item to the store" );
  test_assert_string( list_gets(store, 1), "B", "Should add the second item to the store" );

  list_destroy(store);
  sink_destroy(sink);
}

static void test_transform_sink() {
  Stream *stream = stream_new();

  List *store = list_new(ListAny);
  stream_fork(stream, memory_sink(store));

  Sink *sink = transform_sink(stream, prefix_transformer, "prefix:");
  sink->consume("A", sink->options);
  sink->consume("B", sink->options);

  test_assert_int( list_size(store), 2, "Should pass through all items" );
  test_assert_string( list_gets(store, 0), "prefix:A", "Should transform first item");
  test_assert_string( list_gets(store, 1), "prefix:B", "Should transform second item");

  list_destroy(store);
  sink_destroy(sink);
}

static void test_split_sink() {
  Stream *pass = stream_new();
  Stream *fail = stream_new();

  List *passes = list_new(ListAny);
  List *fails = list_new(ListAny);

  stream_fork(pass, memory_sink(passes));
  stream_fork(fail, memory_sink(fails));

  Sink *will_pass = split_sink(pass, fail, always_true, NULL);
  Sink *will_fail = split_sink(pass, fail, always_false, NULL);

  will_pass->consume("A", will_pass->options);
  will_fail->consume("B", will_fail->options);
  will_pass->consume("C", will_pass->options);

  test_assert_int( list_size(passes), 2, "Should split items that satisfy the condition" );
  test_assert_int( list_size(fails), 1, "Should split items that fail the condition" );

  test_assert_string( list_gets(passes, 0), "A", "Should split first item" );
  test_assert_string( list_gets(fails, 0), "B", "Should split second item" );
  test_assert_string( list_gets(passes, 1), "C", "Should split third item" );

  list_destroy(passes);
  list_destroy(fails);
  sink_destroy(will_pass);
}

static void test_sink_compare() {
  List *store1 = list_new(ListAny);
  List *store2 = list_new(ListAny);

  Sink *memory_sink1 = memory_sink(store1);
  Sink *memory_sink2 = memory_sink(store2);
  Sink *memory_sink3 = memory_sink(store1);

  Stream *stream = stream_new();
  Sink *other_sink = transform_sink(null_transformer, stream, NULL);

  test_assert_int( sink_compare(memory_sink1, memory_sink3), 0, "Should return true when sinks are the same" );
  test_assert_int( sink_compare(memory_sink1, memory_sink2), -1, "Should return false when options is different" );
  test_assert_int( sink_compare(memory_sink1, other_sink), -1, "Should return false when consumer is different");
}

static const char *prefix_transformer(const char *data, const char *options) {
  int total_length = strlen(data) + strlen(options);
  char *result = (char *)malloc(sizeof(char) * total_length);

  sprintf(result, "%s%s", options, data);

  return result;
}

static void *null_transformer(void *data, void *options) {
  return data;
}

static int always_false(void *data, void *options) {
  return 0;
}

static int always_true(void *data, void *options) {
  return 1;
}
