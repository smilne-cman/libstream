#include <stdio.h>
#include <stdlib.h>
#include <libcollection/list.h>
#include <libcollection/compare.h>
#include "sinks.h"
#include "types.h"
#include "consumers.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
static void destroy_split_sink(SplitSink *sink);
static void destroy_transform_sink(TransformSink *sink);

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/
Sink *sink_new(StreamConsumer consumer, void *options, SinkDestroyer destroy) {
  Sink *sink = (Sink *)malloc(sizeof(Sink));

  sink->consume = consumer;
  sink->options = options;
  sink->destroy = destroy;

  return sink;
}

Sink *split_sink(Stream *pass, Stream *fail, ConditionTransformer condition, void *options) {
  return sink_new(split_sink_consumer, split_sink_options(condition, pass, fail, options), destroy_split_sink);
}

Sink *transform_sink(Stream *output, StreamTransformer transformer, void *options) {
  return sink_new(transform_sink_consumer, transform_sink_options(transformer, output, options), destroy_transform_sink);
}

Sink *memory_sink(List *store) {
  return sink_new(memory_sink_consumer, store, NULL);
}

TransformSink *transform_sink_options(StreamTransformer transformer, Stream *output, void *options) {
  TransformSink *data = (TransformSink *)malloc(sizeof(TransformSink));

  data->transformer = transformer;
  data->output = output;
  data->options = options;

  return data;
}

SplitSink *split_sink_options(ConditionTransformer condition, Stream *pass, Stream *fail, void *options) {
  SplitSink *data = (SplitSink *)malloc(sizeof(SplitSink));

  data->condition = condition;
  data->options = options;
  data->pass = pass;
  data->fail = fail;

  return data;
}

int sink_compare(void *value, void *target) {
  Sink *sink1 = (Sink *)value;
  Sink *sink2 = (Sink *)target;

  if (void_compare(sink1->consume, sink2->consume)) {
    return -1;
  }

  if (void_compare(sink1->options, sink2->options)) {
    return -1;
  }

  return 0;
}

void sink_destroy(Sink *sink) {
  if (sink->destroy != NULL) {
    sink->destroy(sink->options);
  }

  free(sink);
}

static void destroy_split_sink(SplitSink *sink) {
  free(sink->pass);
  free(sink->fail);
  free(sink);
}

static void destroy_transform_sink(TransformSink *sink) {
  free(sink->output);
  free(sink);
}
