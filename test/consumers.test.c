#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libtest/libtest.h>
#include <libcollection/list.h>
#include "consumers.h"
#include "stream.h"
#include "sinks.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
static void test_memory_consumer();
static void test_merge_consumer();
static void test_split_consumer();
static void test_transform_consumer();

static int always_false(void *data, void *options);
static int always_true(void *data, void *options);
static const char *prefix_transformer(const char *data, const char *options);

/* Global Variables ***********************************************************/

int main(int argc, char *argv[]) {
  test_init("consumers", argc, argv);

  test("Memory Consumer", test_memory_consumer);
  test("Merge Consumer", test_merge_consumer);
  test("Split Consumer", test_split_consumer);
  test("Transform Consumer", test_transform_consumer);

  return test_complete();
}

static void test_memory_consumer() {
  List *store = list_new(ListAny);

  memory_sink_consumer("A", store);
  memory_sink_consumer("B", store);
  memory_sink_consumer("C", store);

  test_assert_int( list_size(store), 3, "Should add each item to the memory store" );
  test_assert_string( list_gets(store, 0), "A", "Should pass first item to memory store");
  test_assert_string( list_gets(store, 1), "B", "Should pass second item to memory store");
  test_assert_string( list_gets(store, 2), "C", "Should pass third item to memory store");

  list_destroy(store);
}

static void test_merge_consumer() {
  Stream *stream = stream_new();

  List *store = list_new(ListAny);
  stream_fork(stream, memory_sink(store));

  merge_sink_consumer("A", stream);
  merge_sink_consumer("B", stream);
  merge_sink_consumer("C", stream);

  test_assert_int( list_size(store), 3, "Should pass items through to destination stream" );
  test_assert_string( list_gets(store, 0), "A", "Should pass first item" );
  test_assert_string( list_gets(store, 1), "B", "Should pass second item" );
  test_assert_string( list_gets(store, 2), "C", "Should pass third item" );

  list_destroy(store);
  stream_destroy(stream);
}

static void test_split_consumer() {
  Stream *pass = stream_new();
  Stream *fail = stream_new();

  List *passes = list_new(ListAny);
  List *fails = list_new(ListAny);

  stream_fork(pass, memory_sink(passes));
  stream_fork(fail, memory_sink(fails));

  SplitSink *will_pass = split_sink_options(always_true, pass, fail, NULL);
  SplitSink *will_fail = split_sink_options(always_false, pass, fail, NULL);

  split_sink_consumer("A", will_pass);
  split_sink_consumer("B", will_fail);
  split_sink_consumer("C", will_pass);

  test_assert_int( list_size(passes), 2, "Should pass the correct number of items to pass stream" );
  test_assert_int( list_size(fails), 1, "Should pass the correct number of items to fail stream" );

  test_assert_string( list_gets(passes, 0), "A", "Should pass the first item to the correct stream" );
  test_assert_string( list_gets(fails, 0), "B", "Should pass the second item to the correct stream" );
  test_assert_string( list_gets(passes, 1), "C", "Should pass the third item to the correct stream" );

  list_destroy(passes);
  list_destroy(fails);
  stream_destroy(pass);
  stream_destroy(fail);
}

static void test_transform_consumer() {
  Stream *stream = stream_new();

  List *store = list_new(ListAny);
  stream_fork(stream, memory_sink(store));

  TransformSink *options = transform_sink_options(prefix_transformer, stream, "prefix:");

  transform_sink_consumer("A", options);
  transform_sink_consumer("B", options);
  transform_sink_consumer("C", options);

  test_assert_int( list_size(store), 3, "Should apply transformer to all items" );
  test_assert_string( list_gets(store, 0), "prefix:A", "Should transform first item" );
  test_assert_string( list_gets(store, 1), "prefix:B", "Should transform second item" );
  test_assert_string( list_gets(store, 2), "prefix:C", "Should transform third item" );

  list_destroy(store);
  stream_destroy(stream);
}

static int always_false(void *data, void *options) {
  return 0;
}

static int always_true(void *data, void *options) {
  return 1;
}

static const char *prefix_transformer(const char *data, const char *options) {
  int total_length = strlen(data) + strlen(options);
  char *result = (char *)malloc(sizeof(char) * total_length);

  sprintf(result, "%s%s", options, data);

  return result;
}
