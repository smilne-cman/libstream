#include <stdio.h>
#include <stdlib.h>
#include <libtest/libtest.h>
#include <libcollection/list.h>
#include "stream.h"
#include "sinks.h"

/* Types **********************************************************************/
typedef enum Gender {
  Male,
  Female
} Gender;

typedef struct TestRecord {
  int id;
  const char *name;
  int age;
  Gender gender;
  int children;
} TestRecord;

/* Prototypes *****************************************************************/
static void test_basic();
static void test_transform();
static void test_filter();
static void test_split();
static void test_join();
static void test_batch();
static void test_advanced();

static const char *prefix_transformer(const char *data, const char *options);
static int starts_with(const char *data, const char *options);
static TestRecord *record_new(int id, const char *name, int age, Gender gender, int children);
static TestRecord *add_id(TestRecord *data, void *options);
static int is_adult(TestRecord *data, void *options);
static int is_male(TestRecord *data, void *options);
static int has_no_chidren(TestRecord *data, void *options);
static const char *record_to_string(TestRecord *data, void *options);

/* Global Variables ***********************************************************/
static int global_id = 0;

int main(int argc, char *argv[]) {
  test_init("stream", argc, argv);

  test("Basic Stream", test_basic);
  test("Transforming Stream", test_transform);
  test("Filtered Stream", test_filter);
  test("Split Stream", test_split);
  test("Joined Streams", test_join);
  test("Batched Stream", test_batch);
  test("Advanced Stream", test_advanced);

  return test_complete();
}

static void test_basic() {
  Stream *stream = stream_new();

  List *store = list_new(ListAny);
  stream_fork(stream, memory_sink(store));

  stream_add(stream, "helloworld");

  test_assert_string( list_gets(store, 0), "helloworld", "Should pass item downstream" );

  list_destroy(store);
  stream_destroy(stream);
}

static void test_transform() {
  Stream *source = stream_new();
  Stream *transformed = stream_transform(source, prefix_transformer, "test:");

  List *store = list_new(ListAny);
  stream_fork(transformed, memory_sink(store));

  stream_add(source, "hello");
  stream_add(source, "world");

  test_assert_int( list_size(store), 2, "Should pass both items through the transformer" );
  test_assert_string( list_gets(store, 0), "test:hello", "Should transform the first item" );
  test_assert_string( list_gets(store, 1), "test:world", "Should transform the second item" );

  list_destroy(store);
  stream_destroy(source);
}

static void test_filter() {
  Stream *source = stream_new();
  Stream *filtered = stream_filter(source, starts_with, "B");

  List *store = list_new(ListAny);
  stream_fork(filtered, memory_sink(store));

  stream_add(source, "A");
  stream_add(source, "B");

  test_assert_int( list_size(store), 1, "Should only pass items that pass the condition downstream" );
  test_assert_string( list_gets(store, 0), "B", "Should pass the second item" );

  list_destroy(store);
  stream_destroy(source);
}

static void test_split() {
  Stream *source = stream_new();
  List *streams = stream_split(source, starts_with, "A");
  Stream *stream1 = list_get(streams, 0);
  Stream *stream2 = list_get(streams, 1);

  List *store1 = list_new(ListAny);
  List *store2 = list_new(ListAny);

  stream_fork(stream1, memory_sink(store1));
  stream_fork(stream2, memory_sink(store2));

  stream_add(source, "A");
  stream_add(source, "B");

  test_assert_int( list_size(store1), 1, "Should only pass items that pass the condition to the pass stream" );
  test_assert_string( list_gets(store1, 0), "A", "Should pass the first item" );

  test_assert_int( list_size(store2), 1, "Should only pass items that fail the condition to the fail stream" );
  test_assert_string( list_gets(store2, 0), "B", "Should pass the second item" );

  list_destroy(store1);
  list_destroy(store2);
  stream_destroy(source);
}

static void test_join() {
  Stream *source1 = stream_new();
  Stream *source2 = stream_new();
  Stream *stream = stream_join(source1, source2);

  List *store = list_new(ListAny);
  stream_fork(stream, memory_sink(store));

  stream_add(source1, "A");
  stream_add(source2, "B");
  stream_add(source1, "C");

  test_assert_int( list_size(store), 3, "Should pass all items from both streams downstream" );
  test_assert_string( list_gets(store, 0), "A", "Should pass the first item" );
  test_assert_string( list_gets(store, 1), "B", "Should pass the second item" );
  test_assert_string( list_gets(store, 2), "C", "Should pass the third item" );

  list_destroy(store);
  stream_destroy(source1);
  stream_destroy(source2);
  stream_destroy(stream);
}

static void test_batch() {
  Stream *source = stream_new();
  Stream *batched = stream_batch(source, 3);

  List *store = list_new(ListAny);
  stream_fork(batched, memory_sink(store));

  stream_add(source, "A");
  stream_add(source, "B");
  stream_add(source, "C");
  stream_add(source, "1");
  stream_add(source, "2");
  stream_add(source, "3");

  test_assert_int( list_size(store), 2, "Should emit two batches");

  List *batch1 = list_get(store, 0);
  List *batch2 = list_get(store, 1);

  test_assert_int( list_size(batch1), 3, "Should emit three items in the first batch" );
  test_assert_int( list_size(batch2), 3, "Should emit three items in the second batch" );
  test_assert_string( list_gets(batch1, 0), "A", "Should emit first item in first batch" );
  test_assert_string( list_gets(batch1, 1), "B", "Should emit second item in first batch" );
  test_assert_string( list_gets(batch1, 2), "C", "Should emit third item in first batch" );

  test_assert_string( list_gets(batch2, 0), "1", "Should emit fourth item in second batch" );
  test_assert_string( list_gets(batch2, 1), "2", "Should emit fifth item in second batch" );
  test_assert_string( list_gets(batch2, 2), "3", "Should emit sixth item in second batch" );

  list_destroy(batch1);
  list_destroy(batch2);
  list_destroy(store);
  stream_destroy(source);
}

static void test_advanced() {
  Stream *source = stream_new();
  Stream *stage1 = stream_transform(source, add_id, NULL);
  Stream *stage2 = stream_filter(stage1, is_adult, NULL);

  List *stage3 = stream_split(stage2, is_male, NULL);
  Stream *men = list_get(stage3, 0);
  Stream *women = list_get(stage3, 1);

  Stream *women_without_children = stream_filter(women, has_no_chidren, NULL);

  Stream *stage4 = stream_join(men, women_without_children);
  Stream *stage5 = stream_transform(stage4, record_to_string, NULL);

  List *store = list_new(ListAny);
  stream_fork(stage5, memory_sink(store));

  stream_add(source, record_new(0, "Steven Milne", 29, Male, 0));
  stream_add(source, record_new(0, "Shanice Mackay", 14, Female, 0));
  stream_add(source, record_new(0, "Shane Milne", 27, Male, 1));
  stream_add(source, record_new(0, "Melanie Pearson", 48, Female, 1));
  stream_add(source, record_new(0, "Tyler Milne", 1, Male, 0));
  stream_add(source, record_new(0, "Victoria Baird", 29, Female, 0));

  test_assert_int( list_size(store), 3, "Should pass the correct number of items through the stream" );
  test_assert_string( list_gets(store, 0), "0: Mr Steven Milne", "Should pass transformed first item");
  test_assert_string( list_gets(store, 1), "2: Mr Shane Milne", "Should pass transformed third item");
  test_assert_string( list_gets(store, 2), "5: Ms Victoria Baird", "Should pass transformed sixth item");

  list_destroy(store);
  list_destroy(stage3);
  stream_destroy(source);
}

static const char *prefix_transformer(const char *data, const char *options) {
  int total_length = strlen(data) + strlen(options);
  char *result = (char *)malloc(sizeof(char) * total_length);

  sprintf(result, "%s%s", options, data);

  return result;
}

static int starts_with(const char *data, const char *options) {
  return data[0] == options[0];
}

static TestRecord *record_new(int id, const char *name, int age, Gender gender, int children) {
  TestRecord *record = (TestRecord *)malloc(sizeof(TestRecord));

  record->id = id;
  record->name = name;
  record->age = age;
  record->gender = gender;
  record->children = children;

  return record;
}

static TestRecord *add_id(TestRecord *data, void *options) {
  data->id = global_id++;

  return data;
}

static int is_adult(TestRecord *data, void *options) {
  return data->age >= 18;
}

static int is_male(TestRecord *data, void *options) {
  return data->gender == Male;
}

static int has_no_chidren(TestRecord *data, void *options) {
  return !data->children;
}

static const char *record_to_string(TestRecord *data, void *options) {
  char *result = (char *)malloc(sizeof(char) * 256);

  sprintf(result, "%i: %s %s", data->id, data->gender == Male ? "Mr" : "Ms", data->name);

  return result;
}
