#ifndef __sinks__
#define __sinks__

/* Includes *******************************************************************/
#include "types.h"

/* Types **********************************************************************/

/* Macros *********************************************************************/

/* Global Functions ***********************************************************/
Sink *sink_new(StreamConsumer consumer, void *options, SinkDestroyer destroy);

Sink *split_sink(Stream *pass, Stream *fail, ConditionTransformer condition, void *options);
Sink *transform_sink(Stream *output, StreamTransformer transformer, void *options);
Sink *memory_sink(List *store);

TransformSink *transform_sink_options(StreamTransformer transformer, Stream *output, void *options);
SplitSink *split_sink_options(ConditionTransformer condition, Stream *pass, Stream *fail, void *options);

int sink_compare(void *value, void *target);
void sink_destroy(Sink *sink);

#endif
