#include <stdio.h>
#include <stdlib.h>
#include <libcollection/list.h>
#include "transformers.h"
#include "types.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/
void *filter_transformer(void *data, FilterTransformerOptions *options) {
  if (options->condition(data, options->options)) {
    return data;
  }

  return NULL;
}

List *batch_transformer(void *data, BatchTransformerOptions *options) {
  int current_size = list_size(options->batch) + 1;

  list_add(options->batch, data);
  if (current_size == options->size) {
    List *finished = options->batch;

    options->batch = list_new(ListAny);

    return finished;
  }

  return NULL;
}

FilterTransformerOptions *filter_transformer_options(ConditionTransformer condition, void *options) {
  FilterTransformerOptions *data = (FilterTransformerOptions *)malloc(sizeof(FilterTransformerOptions));

  data->condition = condition;
  data->options = options;

  return data;
}

BatchTransformerOptions *batch_transformer_options(int size) {
  BatchTransformerOptions *data = (BatchTransformerOptions *)malloc(sizeof(BatchTransformerOptions));

  data->batch = list_new(ListAny);
  data->size = size;

  return data;
}
