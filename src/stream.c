#include <stdio.h>
#include <stdlib.h>
#include "stream.h"
#include "sinks.h"
#include "consumers.h"
#include "transformers.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/
Stream *stream_new() {
  Stream *stream = (Stream *)malloc(sizeof(Stream));

  stream->forks = set_new(sink_compare);

  return stream;
}

void stream_add(Stream *stream, void *data) {
  if (data == NULL) return;

  Iterator *iterator = set_iterator(stream->forks);

  while (has_next(iterator)) {
    Sink *sink = (Sink *)next(iterator);

    sink->consume(data, sink->options);
  }

  iterator_destroy(iterator);
}

void stream_fork(Stream *stream, Sink *sink) {
  set_add(stream->forks, sink);
}

Stream *stream_transform(Stream *stream, StreamTransformer transformer, void *options) {
  Stream *transformed = stream_new();

  stream_fork(stream, transform_sink(transformed, transformer, options));

  return transformed;
}

Stream *stream_filter(Stream *stream, ConditionTransformer condition, void *options) {
  return stream_transform(stream, filter_transformer, filter_transformer_options(condition, options));
}

Stream *stream_join(Stream *stream1, Stream *stream2) {
  Stream *merge = stream_new();

  stream_fork(stream1, sink_new(merge_sink_consumer, merge, NULL));
  stream_fork(stream2, sink_new(merge_sink_consumer, merge, NULL));

  return merge;
}

Stream *stream_batch(Stream *stream, int size) {
  return stream_transform(stream, batch_transformer, batch_transformer_options(size));
}

List *stream_split(Stream *stream, ConditionTransformer condition, void *options) {
  Stream *pass = stream_new();
  Stream *fail = stream_new();
  List *streams = list_new(ListAny);

  list_add(streams, pass);
  list_add(streams, fail);

  stream_fork(stream, split_sink(pass, fail, condition, options));

  return streams;
}

void stream_destroy(Stream *stream) {
  Iterator *iterator = set_iterator(stream->forks);
  while (has_next(iterator)) {
    sink_destroy(next(iterator));
  }

  iterator_destroy(iterator);
  set_destroy(stream->forks);
  free(stream);
}
