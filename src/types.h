#ifndef __types__
#define __types__

/* Includes *******************************************************************/
#include <libcollection/set.h>

/* Types **********************************************************************/
typedef void *(*StreamTransformer)(void *data, void *options);
typedef int (*ConditionTransformer)(void *data, void *options);
typedef void (*StreamConsumer)(void *data, void *options);
typedef void (*SinkDestroyer)(void *options);

typedef struct Stream {
  Set *forks;
} Stream;

typedef struct Sink {
  StreamConsumer consume;
  void *options;
  SinkDestroyer destroy;
} Sink;

typedef struct TransformSink {
  StreamTransformer transformer;
  Stream *output;
  void *options;
} TransformSink;

typedef struct SplitSink {
  ConditionTransformer condition;
  Stream *pass;
  Stream *fail;
  void *options;
} SplitSink;

typedef struct FilterTransformerOptions {
  ConditionTransformer condition;
  void *options;
} FilterTransformerOptions;

typedef struct BatchTransformerOptions {
  List *batch;
  int size;
} BatchTransformerOptions;

/* Macros *********************************************************************/

/* Global Functions ***********************************************************/

#endif
