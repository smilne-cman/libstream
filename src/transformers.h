#ifndef __transformers__
#define __transformers__

/* Includes *******************************************************************/
#include "types.h"

/* Types **********************************************************************/

/* Macros *********************************************************************/

/* Global Functions ***********************************************************/
void *filter_transformer(void *data, FilterTransformerOptions *options);
List *batch_transformer(void *data, BatchTransformerOptions *options);

FilterTransformerOptions *filter_transformer_options(ConditionTransformer condition, void *options);
BatchTransformerOptions *batch_transformer_options(int size);

#endif
