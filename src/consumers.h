#ifndef __consumers__
#define __consumers__

/* Includes *******************************************************************/
#include "types.h"

/* Types **********************************************************************/

/* Macros *********************************************************************/

/* Global Functions ***********************************************************/
void memory_sink_consumer(void *data, List *store);
void merge_sink_consumer(void *data, Stream *merge);
void split_sink_consumer(void *data, SplitSink *options);
void transform_sink_consumer(void *data, TransformSink *options);

#endif
